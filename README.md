# My Vim notes

## S = save all

### Vim
```
nnoremap   S :wa<CR>
```

### Spacemacs
```
  (define-key evil-normal-state-map (kbd "S") 'evil-write-all)
```

### Visual Studio Code

 ```
    "vim.normalModeKeyBindings": [  
        {  
            "before": [  
                "S"
            ],
            "after": [
                ":", 
                "w",
                "a",
                "Enter"
            ]
        },
    ],
```

### Visual Studio VSVim

```
nnoremap   S :wa<CR>
```



## reverse ; and :

### Vim
```
nnoremap : ;
nnoremap ; :
vnoremap ; :
vnoremap : ;
```

### Spacemacs
```
  (define-key spacemacs-buffer-mode-map (kbd ";") 'evil-ex)
  (define-key spacemacs-buffer-mode-map (kbd ":") 'evil-repeat-find-char)
  (define-key evil-normal-state-map (kbd ";") 'evil-ex)
  (define-key evil-normal-state-map (kbd ":") 'evil-repeat-find-char)
```

### Visual Studio Code
```
  "vim.normalModeKeyBindings": [
      {
            "before": [
                ";"
            ],
            "after": [
                ":"
            ]
        },
      {
            "before": [
                ":"
            ],
            "after": [
                ";"
            ]
        },
    ]
```


### Visual Studio VSVim

```
nnoremap : ;
nnoremap ; :
vnoremap ; :
vnoremap : ;
```
